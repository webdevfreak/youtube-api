# YouTube API
This module allows you to get views count, likes count, dislikes count,
favorite count, comments count for a provided YouTube video url.
YouTube API - https://developers.google.com/youtube/v3/getting-started

# Requirements
This module communicates with YouTube API via CURL and do not need any library
files downloaded.
You should create API Key and make sure 'YouTube Data API' service is enabled.
Follow instruction here to create an API Key;
https://developers.google.com/youtube/v3/getting-started

# Installation
To install, copy the youtube_statistics folder in your 'sites/all/modules'
directory.
Go to Administer -> modules and look for YOUTUBE -> YouTube API and enable this
module.

# Configuration
Go to Administer -> Configuration -> System -> YouTube API Key.
Enter valid 'YouTube API Key' and save.
Please make sure 'YouTube Data API' service is enabled.

# Current maintainer
Baber Abbasi - https://www.drupal.org/user/3385074
